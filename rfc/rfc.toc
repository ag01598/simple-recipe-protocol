\babel@toc {USenglish}{}\relax 
\contentsline {section}{\numberline {1}Introduction}{3}{section.1}%
\contentsline {subsection}{\numberline {1.1}Terminology}{3}{subsection.1.1}%
\contentsline {paragraph}{SRP}{3}{subsection.1.1}%
\contentsline {paragraph}{Endpoint}{3}{subsection.1.1}%
\contentsline {paragraph}{Client}{3}{subsection.1.1}%
\contentsline {paragraph}{Server}{3}{subsection.1.1}%
\contentsline {paragraph}{Packet}{3}{subsection.1.1}%
\contentsline {paragraph}{Address}{3}{subsection.1.1}%
\contentsline {paragraph}{Header}{3}{subsection.1.1}%
\contentsline {paragraph}{Full Application}{3}{subsection.1.1}%
\contentsline {section}{\numberline {2}Overview}{3}{section.2}%
\contentsline {section}{\numberline {3}Packet}{4}{section.3}%
\contentsline {subsection}{\numberline {3.1}Header}{4}{subsection.3.1}%
\contentsline {subsubsection}{\numberline {3.1.1}Fields}{4}{subsubsection.3.1.1}%
\contentsline {paragraph}{Message Number}{4}{subsubsection.3.1.1}%
\contentsline {paragraph}{Message Type}{4}{subsubsection.3.1.1}%
\contentsline {paragraph}{Checksum}{4}{subsubsection.3.1.1}%
\contentsline {paragraph}{Number of parts}{4}{subsubsection.3.1.1}%
\contentsline {paragraph}{Progress Of Message}{4}{subsubsection.3.1.1}%
\contentsline {paragraph}{Data}{4}{subsubsection.3.1.1}%
\contentsline {subsection}{\numberline {3.2}Message Types}{5}{subsection.3.2}%
\contentsline {subsection}{\numberline {3.3}Checksum}{5}{subsection.3.3}%
\contentsline {subsection}{\numberline {3.4}Data}{5}{subsection.3.4}%
\contentsline {subsubsection}{\numberline {3.4.1}Ack, Goodbye, Error, Not found messages}{5}{subsubsection.3.4.1}%
\contentsline {subsubsection}{\numberline {3.4.2}Request List messages}{5}{subsubsection.3.4.2}%
\contentsline {subsubsection}{\numberline {3.4.3}Request Id messages}{5}{subsubsection.3.4.3}%
\contentsline {subsubsection}{\numberline {3.4.4}List Response messages}{6}{subsubsection.3.4.4}%
\contentsline {paragraph}{Id}{6}{subsubsection.3.4.4}%
\contentsline {paragraph}{Name}{6}{subsubsection.3.4.4}%
\contentsline {subsubsection}{\numberline {3.4.5}Recipe response messages}{6}{subsubsection.3.4.5}%
\contentsline {subsubsection}{\numberline {3.4.6}Hello Message}{6}{subsubsection.3.4.6}%
\contentsline {section}{\numberline {4}Communication Flow}{6}{section.4}%
\contentsline {subsection}{\numberline {4.1}Start}{7}{subsection.4.1}%
\contentsline {subsection}{\numberline {4.2}Data}{7}{subsection.4.2}%
\contentsline {subsection}{\numberline {4.3}Goodbye}{7}{subsection.4.3}%
\contentsline {subsection}{\numberline {4.4}Error}{8}{subsection.4.4}%
\contentsline {subsection}{\numberline {4.5}Splitting}{8}{subsection.4.5}%
\contentsline {subsubsection}{\numberline {4.5.1}Server bound}{8}{subsubsection.4.5.1}%
\contentsline {subsubsection}{\numberline {4.5.2}Client Bound}{8}{subsubsection.4.5.2}%
\contentsline {section}{\numberline {5}References}{9}{section.5}%
