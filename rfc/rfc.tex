\documentclass[10pt]{article}
\usepackage[USenglish]{babel}
\usepackage[useregional]{datetime2}
\usepackage{lipsum}
\usepackage{hyperref}
\usepackage{fancyvrb}
\usepackage[style=numeric,sorting=none,backend=biber]{biblatex}

\addbibresource{rfc.bib}

\DTMlangsetup[en-US]{showdayofmonth=false}

\newcommand{\headerSize}{8 bytes}

\newcommand{\rfcId}{I-D}
\newcommand{\defPort}{42069}
\newcommand{\rfcTitle}{Simple Recipe Protocol}
\newcommand{\rfcSimpTitle}{SRP}
%\newcommand{\rfcAuthor}{Group 15}
\newcommand{\rfcAuthor}{
A. Solomonides \\
A. Henriques \\
O. Venables \\
P. Rodriguez
}
\newcommand{\rfcAuthorFooter}{Group 15}
\newcommand{\rfcDate}{\today}
\newcommand{\rfcInstitution}{University of Surrey}

% TABLE OF CONTENT
\usepackage{tocloft}
\renewcommand{\cftsecleader}{\cftdotfill{\cftdotsep}}
\renewcommand{\cftsubsecleader}{\cftdotfill{\cftdotsep}}
\renewcommand{\cftsubsubsecleader}{\cftdotfill{\cftdotsep}}
\renewcommand{\contentsname}{Table of Content}

% MARGINS
\usepackage{titlesec}
\titlelabel{\thetitle.\quad}
\usepackage{geometry} 
\geometry{
	a4paper,
	left=30mm,
	top=30mm,
	bottom=30mm,
	right=30mm
}
\setlength{\leftskip}{17pt}

% HEADER AND FOOTER
\usepackage{lastpage}
\usepackage{fancyhdr}
\pagestyle{fancyplain}
\fancyhead{}
\fancyfoot{}
\fancyhead[L]{RFC \rfcId}
\fancyhead[C]{\rfcTitle}
\fancyhead[R]{\rfcDate}
\fancyfoot[L]{\rfcAuthorFooter} 
\fancyfoot[C]{Standards Track} 
\fancyfoot[R]{[Page \thepage] \\} 
\renewcommand{\headrulewidth}{0pt} 
\renewcommand{\footrulewidth}{0pt} 
\setlength{\headheight}{13.6pt}

% FIRST PAGE
\usepackage{multicol}

% FONT
\usepackage{inconsolata}
\renewcommand{\familydefault}{\ttdefault}

\begin{document}

\begin{multicols}{2}
	\begin{flushleft}
		Independent Submission\\
		Request for comments \rfcId
	\end{flushleft}
\columnbreak
	\begin{flushright}
		\rfcAuthor \\
		\rfcInstitution \\
		\rfcDate
	\end{flushright}
\end{multicols}

\vspace{1in} { \center \rfcTitle \\ } \vspace{1in}

\begin{abstract}
	This document describes the core of the \rfcTitle{}. \rfcTitle{} provides a way
	to simple exchange recipes.
\end{abstract}

\pagebreak

\tableofcontents
\pagebreak

\section{Introduction}{
	\raggedright
	\rfcTitle{} (\rfcSimpTitle{}) is a simple protocol to exchange recipes. 
	This protocol assumes that the User Datagram Protocol(UDP) \cite{udp} 
	is used as the underlying protocol.

	\medskip

	This protocol provides a procedure for application programs to send recipes
	to other applications with a minimum  of protocol mechanism. The protocol 
	is builds on top of UDP\cite{udp}, and adds delivery and duplicate protection.  

	\subsection{Terminology}{
		\begin{flushleft}
		In this document, the key words "MUST", "MUST NOT", "REQUIRED",
		"SHALL", "SHALL NOT", "SHOULD", "SHOULD NOT", "RECOMMENDED", "MAY",
		and "OPTIONAL" are to be interpreted as described in BCP 14, RFC 2119   [RFC2119].

		Commonly used terms in this document are described below.

		\paragraph{\rfcSimpTitle} The application protocol described by this document.\rfcSimpTitle{} is an acronym.

		\paragraph{Endpoint} An entity that can participate in a \rfcSimpTitle{} connection by generating, receiving, and processing \rfcSimpTitle{} packets. There are only two types of endpoints in \rfcSimpTitle{}: client and server.

		\paragraph{Client} The endpoint that initiates a \rfcSimpTitle{} connection.

		\paragraph{Server} The endpoint that accepts a \rfcSimpTitle{} connection.

		\paragraph{Packet} A complete processable unit of \rfcSimpTitle{} that can be encapsulated in a UDP datagram.

		\paragraph{Address} When used without qualification, the tuple of IP version, IP address, and UDP port number that represents one end of a network path.

		\paragraph{Header} The 1st \headerSize{} of a Packet

		\paragraph{Full Application} An entity that implements a \rfcSimpTitle{} Client and a \rfcSimpTitle{} Server

		\bigskip

		This document uses the terms "\rfcSimpTitle{} packets", "UDP datagrams" to refer to the units of the respective protocols.
		\end{flushleft}
	}
}

\section{Overview}{
\begin{flushleft}

	Full Application contains a Server that will share recipes and Client will be able to search and receive 
	recipes from other servers. Server only instances can be created that only share recipes and client only
	recipes can be created that only receive recipes.

	\medskip

	The clients and servers communicate the recipes by splitting the data into packets(See \ref{packet}). Where each packet must 
	have a header of size \headerSize{}, followed by a data section. Therefore clients and servers must have a
	buffer size with at least 32 bytes.

	\medskip
	
	Servers SHOULD use the default port of \defPort{} and clients if no specific port is specified MUST use the 
	\defPort{} port to try to create a connection with a server.

	\medskip
	
	The burden on validating if a message has been received is on the client, therefore the client SHOULD implement
	mechanisms that allow the re-request of a message in case a certain amount of time has passed, this amount is
	implementation specific, the recommended amount is 1 second.

	\medskip
	
	The communication is started by the client with a Hello Message(See \ref{hello-message}) and the it follows the
	the communication flow(See \ref{communication-flow})

\end{flushleft}
}

\section{Packet}{

\label{packet}
\begin{flushleft}
	
	Packets contain the information for a Server or Client to process the incoming data.

	\medskip

	The Packets start with a header, and then are followed by a data section that contains
	the required information for the message to work.

	\subsection{Header}{
		The header has the essential information for communication to happen successfully.

		All headers MUST have this format:
		\begin{figure}[h]
		\centering
		\begin{BVerbatim}
0         7 8       15 16      23 24       31
+----------+----------+----------+----------+
| Message  | Message  |  Number  | Progress | 
| Number   |   Type   |    Of    |    Of    |
|          |          |   Parts  |  Message |
+----------+----------+----------+----------+
|          |                                |
| Checksum |          Left of as 0          |
|          |                                |
+----------+----------+----------+----------+
| 
|                    data ...
+----------------------...	
		\end{BVerbatim}
		\end{figure}

		\subsubsection{Fields}{

			\paragraph{Message Number} Contains the number of the message that is being send. The 1st message
			SHOULD have the message number $0$

			\paragraph{Message Type} Contains the type of the message that is being send. See \ref{message-types} 
			for possible message types

			\paragraph{Checksum} The check sum MUST be calculated using Longitudinal redundancy check \cite{LRC}(See \ref{checksum}).

			\paragraph{Number of parts} The number of packets that are necessary to be created for the message to be
			send on it's entirety

			\paragraph{Progress Of Message} Which part of the message does this package represent. At the end of 
			communication the number of parts and the progress of message MUST be the same number

			\paragraph{Data} The rest of the data of the message. See \ref{data} for more information on how this
			data must be encoded.

		}

	}

	\subsection{Message Types}{
		\label{message-types}
		The type of a message gives information on how this packet is meant to do.

		There are 9 types of messages, each one with a numeric value associated:

		\begin{itemize}

		\item Ack = 0
		\item Hello = 1
		\item Request Id = 2
		\item Request list = 3
		\item List response = 4
		\item Recipe response = 5
		\item Goodbye = 6
		\item Error = 7
		\item Not found = 8

		\end{itemize}


	}

	\subsection{Checksum}{
		\label{checksum}

		The value of the checksum must be calculated using Longitudinal redundancy check \cite{LRC}.

		\bigskip

		The checksum MUST be calculated over the entire message assuming that the space that is left for 
		the checksum is filled with zeros.\\
		After the checksum is a calculated the result MUST be put back in the header.\\
		At the receiving endpoint, the value MUST be removed from the message the checksum calculated again
		then and compared to the one send in the message.\\
		If the value is the same then the message is valid.

		\bigskip

		If the checksum is not not valid on a client bound message the client should request the same message again.\\
		While if the checksum is not valid on a server bound message the server should respond with an error message.
	}

	\subsection{Data}{
		\label{data}

		The data section of the headers depends on the message type of the packet being send.

		\subsubsection{Ack, Goodbye, Error, Not found messages}{
			This message types SHOULD not have anything on the data section of the packet. 
		}

		\subsubsection{Request List messages}{
			This message should have the name of the desired recipe. The data MUST encoded as UTF-8
		}

		\subsubsection{Request Id messages}{
			This message should have the Id of the desired recipe. The data MUST be encoded as UTF-8
		}

		\subsubsection{List Response messages}{
			This messages sends the list of recipes and the server internal ids for those recipes.
			This message MUST have the list of the recipes in the format:
			\begin{verbatim}
ID:name
			\end{verbatim}
			Different ID, name pairs MUST be separated by a new line character, so a message 
			with 3 ids must be formatted as:
			\begin{verbatim}
ID1:name1\nID2:name2\nID3:name3			
			\end{verbatim}

			And this text data MUST be encoded as UTF-8.

			\paragraph{Id}{
				The Id of a recipe MUST be the a numerical value, or if the recipe is stored in different than the 
				one sending the message then the id MUST be:
			\begin{verbatim}
[id]@[serverIp]
			\end{verbatim}
			}

			\paragraph{Name}{
				The name can not contain any character that would affect the processing of the message. 
				Therefore no title of a recipe can have the following characters: `:', `$\backslash$n',
			}

		}

		\subsubsection{Recipe response messages}{
			This messages should have the text of the recipe. The data MUST be encoded as UTF-8
		}

		\subsubsection{Hello Message}{
			\label{hello-message}
			This message of this type sends the buffer size of the server or the client to the other endpoint.\\
			The size of the buffer MUST be encoded using big byte order.
			\begin{figure}[h]
			\centering
			\begin{BVerbatim}
0         7 8       15 16      23 24       31
+----------+----------+----------+----------+
|             Size of the buffer            |
+----------+----------+----------+----------+
|                 Left as 0                 |
+----------+----------+----------+----------+
			\end{BVerbatim}
			\end{figure}
		}

	}

\end{flushleft}
}

\section{Communication Flow}{
\label{communication-flow}
\begin{flushleft}

	The communication between the server and the client can be split up into 3 parts.

	\bigskip

	The Start with the purpose of sharing some information with each other.

	\bigskip

	A data exchange part where the client and the server exchange the recipes with each other.

	\bigskip

	A Goodbye part where the client and the server end their connection.

	\bigskip

	The server MUST always answer the client with the same message number. The client MUST increasing 
	the number of the message on every new message, if the message is split the number of the message stays
	the same until all the parts of the message is done transmitting.

	\bigskip

	When the message can not fit in one packet the message needs to be split into multiple packets, See \ref{splitting}.\\
	If there is no splitting required then the package parts must be 1, and the progress must also be 1.

	\subsection{Start}{
		
		The communication starts with the client sending hello message, with the buffer size of the
		client on the message

		\begin{figure}[h]
		\centering
		\begin{BVerbatim}
Client                          Server
+----------------------------------->+
            Hello Message
		\end{BVerbatim}
		\end{figure}

		The Server then responds with the hello message and it's buffer size. 

		\begin{figure}[h]
		\centering
		\begin{BVerbatim}
Client                          Server
+<-----------------------------------+
            Hello Message
		\end{BVerbatim}
		\end{figure}

		At this point both the client and the server know the limits of each other can user it to split the 
		packages appropriately.

	}

	\subsection{Data}{

		The client can use the request list message to request a list of recipes from the servejr

		\begin{figure}[h]
		\centering
		\begin{BVerbatim}
Client                          Server
+----------------------------------->+
            Request list
+<-----------------------------------+
            List Response
		\end{BVerbatim}
		\end{figure}

		If the client already knows the Id of a recipe then the 1st step of requesting the list of recipes MAY
		be skipped.

		\begin{figure}[h!]
		\centering
		\begin{BVerbatim}
Client                          Server
+----------------------------------->+
            Request Id
+<-----------------------------------+
            Recipe response
		\end{BVerbatim}
		\end{figure}

		If the client request a id that the server does not contain the server should respond with a Not Found 
		message.

		\begin{figure}[h!]
		\centering
		\begin{BVerbatim}
Client                          Server
+----------------------------------->+
            Request Id
+<-----------------------------------+
            Not Found
		\end{BVerbatim}
		\end{figure}
	}

	\subsection{Goodbye}{

		For the connection to end the client must send a goodbye message to the server that then answers it with a
		goodbye message of it's own. If the client gets an invalid message during this step the client should ignore it.

		\begin{figure}[h!]
		\centering
		\begin{BVerbatim}
Client                          Server
+----------------------------------->+
               Goodbye
+<-----------------------------------+
               Goodbye		
        \end{BVerbatim}
		\end{figure}

		If the server for some reason, like inactivity, or lack of resources needs to disconnect from a client the
		server SHOULD send an error message.
	}

	\subsection{Error}{

		The error message can have any message number, as the connection is supposed to be restarted every-time any
		endpoint receives a error message

		On receiving and error message the receiver MUST assume that the communication is to restarted form the 
		beginning with an hello message.

	}

	\subsection{Splitting}{
		\label{splitting}

		In case the of the receiving buffer is smaller then the total size of the message then must be split
		into multiple packets.\\

		\bigskip

		The number of parts is the total number of parts that are required to send the complete message.\\
		The message progress is how much of the those parts of the message have been sent the this number MUST
		start with one 1 and the message is fully transmitted when the number of the progress is equals the number
		of parts in the message.\\

		\bigskip

		The sender SHOULD send this messages one at the time and wait for an acknowledgement(ACK) from the receiver to 
		send the next message.\\
		The ACK message MUST have the same message number and the Progress of the message of the original message, 
		parts should be kept as 1.

		\bigskip
		
		The burden of checking if the messages are received is still on the client.\\ 
		If the client is the sender then the client MUST wait for the ACK from the server. If the client is the
		receiver then after sending the ACK the client SHOULD wait for the next message and if this does not arrive
		client SHOULD resend the ACK.\\

		If the sender receives an ACK for an older message the sender should ignore that message.

		\subsubsection{Server bound}{

		When the server is the one receiving the message.

		\begin{figure}[h]
		\centering
		\begin{BVerbatim}
Client                          Server
+----------------------------------->+
           Message part 1
+<-----------------------------------+
               ACK 1
+----------------------------------->+
          Message part n + 1
+<-----------------------------------+
              ACK n + 1
+<-----------------------------------+
         Begin server response
		\end{BVerbatim}
		\end{figure}

		}

		\subsubsection{Client Bound}{

		When the client is the one receiving the message.

		\begin{figure}[h!]
		\centering
		\begin{BVerbatim}
Client                          Server
+<-----------------------------------+
           Message part 1
+----------------------------------->+
               ACK 1
+<-----------------------------------+
         Message part n + 1
+----------------------------------->+
              ACK n + 1
		\end{BVerbatim}
		\end{figure}

		\bigskip
		\bigskip
		\bigskip
		\bigskip

		}

	}


\end{flushleft}
}

\section{References}{
	\printbibliography[heading=none]
}

\end{document}