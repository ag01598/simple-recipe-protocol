module main

import math
import encoding.binary
import net

import libsodium

pub const (
	default_buffer_size = 32
	header_size = 8
	local_buffer = 100
	max_depth = 10
	default_port = '42069'
	custom_code_for_timeout = 99
	error_while_reading = 100
	invalid_message_size = 101
	invalid_message_type = 102
	invalid_check_sum = 103
	wrong_message = 104
	error_message = 105
	addr_error = 106
	default_buffers = BufferSizes{target: default_buffer_size, local: local_buffer}
)

pub enum MessageType {
  ack = 0
  hello
  request_id
  request_name
  list_response
  recipe_response
  goodbye
  error
  not_found
  enc_exchange
}

pub struct Message {
	message_type MessageType [required]
	number u8 [required]
	parts u8 [required]
	progress u8 [required]
	data []u8 [required]
	buffers BufferSizes [required]
	encription_bit bool [required]
	keys OptionalKeys [required]
}

pub fn (m Message) str() string {

	mut keys := "None"

	if m.keys is Keys {
		keys = "local only"
		keys_d := m.keys
		if keys_d.target is []u8 {
			keys = '***'
		}
	}

	mut data := m.data
	usable_buffer_size := m.buffers.target - header_size

	if m.parts > 1 {
		if m.progress == m.parts {
			data = m.data[usable_buffer_size*(m.progress-1)..]
		} else {
			data = m.data[usable_buffer_size*(m.progress-1)..usable_buffer_size*(m.progress)]
		}
	}

	pdata := if data.len > 100 {
		data = data[0..100]
		mut datastr := "$data"
		datastr = datastr[..datastr.len - 1]
		datastr += ", ... ]"
		datastr
	} else {
		"$data"
	}

	return 'Message{\n' +
		'\ttype: $m.message_type\n' +
		'\tn: $m.number\n' +
		'\tparts: $m.parts\n' +
		'\tprogress: $m.progress\n' + 
		'\tdata: $pdata\n' + 
		'\tencription_bit: $m.encription_bit\n'+
		'\tkeys: $keys\n' +
		'}'
}

pub struct BufferSizes {
	target u32
	local u32
}

type OptionalKeys = NN | Keys
type MaybeRecieved = []u8 | NN

pub struct NN {}

pub struct Keys {
	target MaybeRecieved
	local libsodium.PrivateKey
}

pub fn calculate_checksum(bytes []u8) u8 {
	mut lrc := 0
	for b in bytes {lrc = (lrc + b) & 0xFF}
	return u8((((lrc ^ 0xFF) + 1) & 0xFF))
}
 
pub fn (m Message) encode() []u8 {
	usable_buffer_size := m.buffers.target - header_size
	mut to_return := []u8{}
	to_return << m.number
	to_return << u8(m.message_type)
	to_return << m.parts
	to_return << m.progress
	// Checksum is 0 during the calculation process
	to_return << u8(0)
	// Left as 0
	to_return << u8(0)
	to_return << u8(0)

	mut to_set := u8(0)

	if m.encription_bit {to_set = u8(1)}

	to_return << to_set

	mut data := m.data

	if m.parts > 1 {
		if m.progress == m.parts {
			data = m.data[usable_buffer_size*(m.progress-1)..]
		} else {
			data = m.data[usable_buffer_size*(m.progress-1)..usable_buffer_size*(m.progress)]
		}
	}

	to_return << data
	to_return[4] = calculate_checksum(to_return)

	return to_return
}

pub fn (m Message) text() string {return m.data.bytestr()}
pub fn (m Message) create_ack() Message {return m.create_new_message(.ack, 1, m.progress, [])}

pub fn (mut m Message) advance(message Message) ?Message {
	if m.number != message.number || m.progress != message.progress || m.parts < message.progress {return none}
	return m.create_new_message(m.message_type, m.parts, m.progress + 1, m.data)
}

pub fn (m Message) add_part(message Message) ?Message {
	if message.number != m.number || message.progress - 1 != m.progress {return none}
	mut data := []u8{}
	data << m.data
	data << message.data
	return m.create_new_message(m.message_type, m.parts, message.progress, data)
}

pub fn (m Message) create_new_message(mt MessageType, parts u8, progress u8, data []u8) Message {
	return Message{
		message_type: mt, 
		parts: parts, 
		progress: progress,
		data: data, 
		number: m.number, 
		buffers: m.buffers
		encription_bit: m.encription_bit
		keys: m.keys
	}
}

fn (m Message) update_buffers(buffers BufferSizes) Message {
	return Message{
		message_type: m.message_type, 
		parts: m.parts, 
		progress: m.progress,
		data: m.data, 
		number: m.number, 
		buffers: buffers
		encription_bit: m.encription_bit
		keys: m.keys
	}
}

fn (m Message) update_keys(keys OptionalKeys) Message {
	return Message {
		message_type: m.message_type, 
		parts: m.parts, 
		progress: m.progress,
		data: m.data, 
		number: m.number, 
		buffers: m.buffers,
		encription_bit: m.encription_bit,
		keys: keys
	}
}

fn (m Message) decrypt() Message {
	if m.keys is Keys {
		keys := m.keys
		if keys.target is []u8 {
			box := libsodium.new_box(m.keys.local, keys.target)
			return m.create_new_message(m.message_type, m.parts, m.progress, box.decrypt(m.data))
		}
	}
	return m
} 

pub fn (m Message) is_done() bool {return m.parts == m.progress}

pub fn create_message_from_bytes(message_type MessageType, number u8, data []u8, buffers BufferSizes, keys OptionalKeys) ?Message {
	if buffers.target < default_buffer_size {return error('invalid buffer size')}

	mut d := data.clone()

	if keys is Keys {
		target := keys.target
		if target is []u8 {
			box := libsodium.new_box(keys.local, target)
			d = box.encrypt(data)
		}
	}

	parts := math.max(math.ceil(data.len/f64(buffers.target - header_size)), 1)
	if parts > 255 {return error('message is to big to be send')}

	return Message{
		message_type: message_type, 
		number: number, 
		parts: u8(parts), 
		progress: 1, 
		data: d, 
		buffers: buffers,
		encription_bit: true,
		keys: keys
	}
}

pub fn (m Message) create_bytes() ?Message {
	return create_message_from_bytes(m.message_type, m.number, m.data, m.buffers, m.keys)
}

pub fn decode(mut bytes []u8, buffers BufferSizes) ?Message {
	if bytes.len < header_size {return error_with_code('invalid message size', invalid_message_size)}
	message_type := bytes[1]

	if message_type < 0 || message_type > u8(MessageType.enc_exchange) {
		return error_with_code('invalid message type', invalid_message_type)
	}

	checksum := bytes[4]
	bytes[4] = 0

	calculated := calculate_checksum(bytes)

	if calculated != checksum {
		return error_with_code('invalid checksum got: $checksum expected: $calculated\n data: $bytes', invalid_check_sum)
	}

	return Message{
		message_type: MessageType(message_type), 
		number: bytes[0], 
		parts: bytes[2], 
		progress: bytes[3], 
		data: bytes[8..],
		buffers: buffers
		encription_bit: (bytes[7] & u8(1)) == u8(1),
		keys: NN{}
	}
}

struct ErrorMessage {
	Error
	message Message
}
pub fn (er ErrorMessage) code () int {return error_message}

struct AddrError {
	Error
	addr net.Addr
	error IError
}
pub fn (er AddrError) code () int {
	if er.error.code() == error_message {return error_message}
	return addr_error
}

pub fn (er AddrError) msg () string {
	return "\n\taddr: $er.addr\n\terror:$er.error"
}

fn wait_for_response_no_type(mut conn &net.UdpConn, buffers BufferSizes, timeout_count int) ?(Message, net.Addr) {
	if timeout_count > max_depth {return error_with_code('max timeout', custom_code_for_timeout)}

	conn.wait_for_read() or {
		// the error code 9 is for network timeout
		if err.code() == 9 {return wait_for_response_no_type(mut conn, buffers, timeout_count + 1)}
		return err
	}

	mut b := []u8{len: int(buffers.local)}

	size, addr := conn.read(mut b) or {return error_with_code("error while reading", error_while_reading)}

	message := decode(mut b[..size], buffers) or {return IError(AddrError{addr: addr, error: IError(err)})}
	if message.message_type == .error {
		return IError(AddrError{addr: addr, error: IError(ErrorMessage{message: message})})
	}
	return message, addr
}

// functions to deal with Hello message
pub fn create_hello_message (bufferSize u32, number u8) ?Message {
	mut bytes := []u8{len: 4}
	binary.big_endian_put_u32(mut bytes, bufferSize)
	return create_message_from_bytes(.hello, number, bytes, BufferSizes{target:default_buffer_size, local:bufferSize}, NN{})
}

pub fn create_enc_message (base Message) ?(Message, libsodium.PrivateKey) {
	my_key := libsodium.new_private_key()

	return base.create_new_message(.enc_exchange, 1, 1, my_key.public_key), my_key
}

pub fn decode_hello(message Message) ?u32 {
	if message.message_type != .hello || message.data.len < 4 {return none}
	return binary.big_endian_u32(message.data[..4])
}