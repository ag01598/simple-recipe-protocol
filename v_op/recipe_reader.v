module main

import os
import regex

struct NotFound {Error}

fn list_recipes_in_folder(base string) [][]string {
	files := os.ls(base) or {return []}
	mut list := [][]string{}
	for file in files {
		if !os.is_file(base + '/' + file) {continue}
		mut re := regex.regex_opt(r"^(\d+)-([\w\s]+).txt$") or {continue}
		start, _ := re.match_string(file)
	    if start >= 0 {
	        list << [re.get_group_by_id(file, 0), re.get_group_by_id(file, 1).to_lower()]
	    }
	}
	return list
}

fn search_by_name(name_in string) string {
	mut name := ".*" + name_in.to_lower() + ".*"
	if name_in.len == 0 {name = ".*"}
	mut final_result := ""
	mut list := list_recipes_in_folder('./recipes')
	for l in list {
		mut re := regex.regex_opt(name) or {continue}
		start, _ := re.match_string(l[1])
		if start >= 0 {final_result += "${l[0]}:${l[1]}\n"}
	}
	if final_result.len == 0 {return ""}
	return final_result[..(final_result.len - 1)]
}

fn search_by_id(id string) ?string {
	mut list := list_recipes_in_folder('./recipes')
	mut name := ""
	for l in list {
		mut re := regex.regex_opt(id) or {continue}
		start, _ := re.match_string(l[0])
		if start >= 0 {
			name = './recipes/${l[0]}-${l[1]}.txt'
			break
		}
	}
	if name.len == 0 {return IError(NotFound{})}
	return os.read_file(name) or {return IError(NotFound{})}
}
