module main 

import time
import net

struct ConnectionManager {
	buffers BufferSizes [required]
	addr net.Addr [required]
mut:
	keys OptionalKeys [required]
	checking_for_enc bool [required]
	sending_msg map[u8]Message [required]
	receiving_msg map[u8]Message [required]
}

fn (mut c ConnectionManager) answer(mut conn &net.UdpConn, m Message, check_parts bool, debug bool) ? {
	if m.message_type == .ack {
		if m.number in c.sending_msg.keys() {
			mut sm := c.sending_msg[m.number]
			if sm.is_done() && sm.parts == m.progress {
				c.sending_msg.delete(m.number)
				return
			}
			sm = sm.advance(m) or {sm}
			sm.send_to(mut conn, c.addr, debug) or {return error("")}
			c.sending_msg[m.number] = sm
			return 
		}
		return error("message already fully recived")
	}
	if m.parts > 1 && check_parts {
		// It already exists
		if m.number in c.receiving_msg.keys() {
			mut sm := c.receiving_msg[m.number]
			sm = sm.add_part(m) or {sm}
			sm.create_ack().send_to(mut conn, c.addr, debug) ?
			if sm.is_done() {
				c.answer(mut conn, sm, false, debug) ?
				c.receiving_msg.delete(m.number)
				return
			}
			c.receiving_msg[m.number] = sm
			return
		} 
		// Adding a new one
		if m.progress == 1 {
			c.receiving_msg[m.number] = m
			m.create_ack().send_to(mut conn, c.addr, debug) ?
			return
		}
		return error("to many parts recived")
	}
	if m.message_type == .request_name {
		if debug {println("searching by name: $m.text()")}
		to_send := search_by_name(m.text())
		msg := create_message_from_bytes(.list_response, m.number, to_send.bytes(), c.buffers, m.keys) ?
		msg.send_to(mut conn, c.addr, debug) ?
		if msg.parts > 1 {c.sending_msg[m.number] = msg}
		return
	}
	if m.message_type == .request_id {
		if debug {println("searching by id: $m.text()")}
		to_send := search_by_id(m.text()) or {
			m.create_new_message(.not_found, 1, 1, []).send_to(mut conn, c.addr, debug) ?
			return
		}
		msg := create_message_from_bytes(.recipe_response, m.number, to_send.bytes(), c.buffers, m.keys) ?
		msg.send_to(mut conn, c.addr, debug) ?
		if msg.parts > 1 {
			c.sending_msg[m.number] = msg
		} 
		return
	}
	return error("unkown message type")
}

//Aditional functuins on Message 

fn (m Message) send_to(mut conn &net.UdpConn, addr net.Addr, debug bool) ? {
	if debug {println(">>> sending:\n$m")}
	conn.write_to(addr, m.encode()) ?
}

pub fn send_error(mut conn &net.UdpConn, addr net.Addr, debug bool) {
	m := create_message_from_bytes(.error, 0, [], default_buffers, NN{}) or {return}
	m.send_to(mut conn, addr, debug) or {}
}

pub fn server(port int, debug_server bool, state chan bool) ? {
	if debug_server {println("running on port: $port")}

	mut s := net.listen_udp(':$port') ?
	mut conns := map[string]ConnectionManager{}

	for {
		// check if the server can stop
		select {
			_ := <-state {
				println("Stopping server")
				break
			}
			time.millisecond * 1 {}
		}

		m, ad := wait_for_response_no_type(mut s, default_buffers, 7) or {
			if err is AddrError {
				if debug_server {
					print("Error in incoming message:")
					println(err)
				}
				if err.code() == error_message && err.addr.str() in conns {conns.delete(err.addr.str())}
				else {send_error(mut s, err.addr, debug_server)}
			}
			continue
		}

		addr := ad.str()

		if m.message_type == .hello && addr in conns.keys() {conns.delete(addr)}
		if m.message_type == .goodbye {
			if debug_server { 
				println("<<< receiving:")
				println(m) 
			}
			if addr in conns.keys() {conns.delete(addr)}
			m.create_new_message(.goodbye, 1, 1, []).send_to(mut s, ad, debug_server) or {}
			continue
		}

		if addr in conns.keys() {

			mut conn := conns[addr]

			mut nm := m.update_buffers(conns[addr].buffers)

			if conn.checking_for_enc {
				keys := conn.keys
				match keys {
					Keys {
						if nm.message_type != .enc_exchange {
							send_error(mut s, ad, debug_server)
							conns.delete(addr)
							continue
						}

						conns[addr].keys = Keys{target: nm.data, local: keys.local}
						conns[addr].checking_for_enc = false

						nm.create_ack().send_to(mut s, ad, debug_server) or {
							send_error(mut s, ad, debug_server)
							conns.delete(addr)
							continue
						}
						continue
					}
					NN {
						send_error(mut s, ad, debug_server)
						conns.delete(addr)
						continue
					}
				}
				continue
			}

			if conn.keys is Keys {nm = nm.update_keys(conn.keys).decrypt()}

			if debug_server { 
				println("<<< receiving:")
				println(nm) 
			}
			if nm.message_type == .goodbye {
				nm.create_new_message(.goodbye, 1, 1, []).send_to(mut s, ad, debug_server) or {}
				conns.delete(addr)
				continue
			}
			conns[addr].answer(mut s, nm, true, debug_server) or {
				println("\n\nError: proccessing the message")
				println(err)
				println("\n\n")
				send_error(mut s, ad, debug_server)
				conns.delete(addr)
			}
			continue
		}
		if debug_server { 
			println("<<< receiving:")
			println(m) 
		}

		// Try to add new connection
		size := decode_hello(m) or {
			send_error(mut s, ad, debug_server)
			continue
		}

		// If the encription bit is set the size of the buffer must be at least 100
		if m.encription_bit && size < 100 {
			send_error(mut s, ad, debug_server)
			continue
		}

		conns[addr] = ConnectionManager{
			buffers: BufferSizes{target: size, local: local_buffer}, 
			addr: ad,
			sending_msg: {},
			receiving_msg: {},
			keys: NN{},
			checking_for_enc: m.encription_bit
		}

		//Create the response
		resp := create_hello_message(local_buffer, m.number) or {
			send_error(mut s, ad, debug_server)
			conns.delete(addr)
			continue
		}
		resp.send_to(mut s, ad, debug_server) or {
			send_error(mut s, ad, debug_server)
			conns.delete(addr)
			continue
		}

		if m.encription_bit {
			if debug_server {println("Client wants enc")}
			resp_enc, private_key := create_enc_message(m) or {
				send_error(mut s, ad, debug_server)
				conns.delete(addr)
				continue
			}

			resp_enc.send_to(mut s, ad, debug_server) or {
				send_error(mut s, ad, debug_server)
				conns.delete(addr)
				continue
			}
			conns[addr].keys = Keys{target: NN{}, local: private_key}
			conns[addr].checking_for_enc = true
		}

		if debug_server { println("SERVER: New connection $size")}
	}
}