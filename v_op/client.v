module main

import os
import net

// Send a message from the client and wait for the full response
pub fn client_side_send_and_wait(mut conn &net.UdpConn, m Message, wait_message_types []MessageType) ?Message {
	mut failures := 0
	for {
		if failures > 10 {return error("faield to send data to the server")}
		mut message := m
		// Send the message
		// If there is only one part then 
		if message.parts == 1 {
			message.send(mut conn) or {continue}
		} else {
			for {
				if failures > 10 {return error("faield to send data to the server")}
				//Try to send the messge 
				message.send(mut conn) or {continue}
				//wait for the response
				resp := message.wait_for_answer(mut conn, [.ack]) or {
					//If the server responded with an error then stop otherwise continue
					if err.code() == error_message {return err}
					failures++
					continue
				}
				// If its done then return
				if message.is_done() {break}
				// Update the message
				message = message.advance(resp) or {continue}
			}
		}
		// Get the response from the server
		mut resp := message.wait_for_answer(mut conn, wait_message_types) or {
			//If the server responded with an error then stop otherwise continue
			if err.code() == error_message {return err}
			if err.code() == invalid_message_type {return error("The server did not respond correctly")}
			failures++
			continue
		}

		// Clean the failures
		failures = 0
		if resp.parts > 1 {
			// check if everything is recived and if so then return that value
			for !resp.is_done() {
				if failures > 10 {return error("faield to get data from the server")}
				// Send ack
				resp.create_ack().send(mut conn) or {continue}
				// Wait for the next part
				temp := resp.wait_for_part(mut conn) or {
					println(err)
					if err.code() == error_message {return err}
					failures++
					continue
				}
				resp = temp
			}
			// Send last ack
			resp.create_ack().send(mut conn) or {}
		}
		return resp.decrypt()
	}
	return none
}

// Wait for a message this code is client specific so I put it here
pub fn (m Message) wait_for_answer(mut conn &net.UdpConn, message_type []MessageType) ?Message {
	message_base, _ := wait_for_response_no_type(mut conn, m.buffers, 0) ?
	message := message_base.update_keys(m.keys)
	// Check for the answer type
	if message_type.len > 0 && !message_type.any(it == message.message_type) {
		return error_with_code('wrong message type', invalid_message_type)
	}
	return message
}

pub fn (m Message) wait_for_part(mut conn &net.UdpConn) ?Message {
	message := m.wait_for_answer(mut conn, [m.message_type]) ?
	// Check if the recovered part is the part that we were expecting
	if message.number != m.number {return error_with_code('wrong message number', wrong_message)}
	return m.add_part(message)
}

pub fn (m Message) send(mut conn &net.UdpConn) ? {
	conn.write(m.encode()) or {return error("Connection failed with the server")}
}

fn check_for_exit (s string) bool {return s == "q" || s == "quit" || s == "e" || s == "exit"}

fn client () ? {
	println("Type q to quit")
	println("Or enter an address to connect")

	for {
		mut line := os.input("q or address >")

		if check_for_exit(line) {
			println("Goodbye")
			return 
		}

		if line == "" {
			println("Please insert a address")
			continue
		}

		// add the default port if did not exit
		if !line.contains(":") {line = '$line:$default_port'}
		if line.starts_with('localhost') {line = line.replace_once('localhost', '127.0.0.1')}

		mut conn := net.dial_udp(line) or { 
			println('Could not find the server $line to connect to')
			continue
		}

		try_to_connect(mut conn) or {
			if err.code() == error_message {
				println("the server answerd with an error please try again later")
				continue
			}
			println(err)
			if err.code() == 98 {return}
			continue
		}
	}
}

fn try_to_connect(mut conn &net.UdpConn) ? {
	mut server_buffer := u32(0)
	mut keys := OptionalKeys(NN{})
	// Try to connect to the server
	for {
	    msg := create_hello_message(local_buffer, 0) ?
	    msg.send(mut conn) or {return error("Connection failed with the server")}
		response := msg.wait_for_answer(mut conn, [.hello]) or {
			if err.code() == invalid_check_sum {continue}
			return err
		}
		server_buffer = decode_hello(response) or {return error("Invalid message from the server")}

		if response.encription_bit {
			server_enc := msg
				.update_buffers(BufferSizes{target: server_buffer, local: local_buffer})
				.wait_for_answer(mut conn, [.enc_exchange]) or 
			{
				if err.code() == invalid_check_sum {continue}
				if err is AddrError {
					println(err.error)
				}
				return err
			}

			resp_enc, key := create_enc_message(server_enc) or {return error("Connection failed with the server")}

			keys = Keys{target: server_enc.data, local: key}

			resp_enc.send(mut conn) or {return error("Connection failed with the server")}

			msg.wait_for_answer(mut conn, [.ack]) or {
				if err.code() == invalid_check_sum {continue}
				return err
			} 
		}

		break
	}
	println("Connected to the server with success")

	buffers := BufferSizes{target:server_buffer, local:local_buffer}
	mut number := u8(1)

	for {
		mut line := os.input("q, name or id >")
		// Check if the user wants to end the connection
		if check_for_exit(line) {
			println("Disconnecting from the server")
			mut good_bye_message := create_message_from_bytes(.goodbye, number, [], buffers, NN{}) or {
				println("Error creating the message!")
				continue
			}
			good_bye_message.send(mut conn) or {return}
			good_bye_message.wait_for_answer(mut conn, []) or {return}
			//If the input is exit then exit the program
			if line == "exit" {return error_with_code("Exiting the client", 98)}
			return
		}
		// Check if the user inputed the name
		// Meaning that the user wants to search by name
		if line.to_lower() == "name" {
			mut name := os.input("name:")

			to_send_message := create_message_from_bytes(.request_name, number, name.bytes(), buffers, keys) or {
				println("failed to create the message to the server")
				continue
			}

			number++

			answer := client_side_send_and_wait(mut conn, to_send_message, [.list_response]) or {
				if err.code() == error_message { return err }
				println(err)
				println("failed to receive message")
				continue
			}

			if answer.text().len == 0 {
				println("No recipies found")
				continue
			}
			println("Found some recipies!")

			recipe_data := answer.text().split('\n')
			mut max_id := 0
			mut max_name := 0
			mut splits_t := [][]string{}

			splits_t << ["Id", "Name"]

			for w in recipe_data {
				if w.len == 0 {continue}
				splits := w.split(':')

				if splits.len != 2 {
					println('Invalid recipe found: "$w"')
					continue
				}
				if splits[0].runes().len > max_id {max_id = splits[0].runes().len}
				if splits[1].runes().len > max_name {max_name = splits[1].runes().len}

				splits_t << splits
			}
			// This function centers the text
			center := fn (a string, n_in int) string {
				if a.runes().len >= n_in {return a}
				mut r := ([]u8{len: (n_in - a.runes().len)/2, init: 32}).bytestr()
				r = r + a + r
				if r.runes().len < n_in {r += " "}
				return r
			}

			max_id += 2
			max_name += 2

			for split in splits_t {
				print(center(split[0], max_id))
				println(center(split[1], max_name))
			}
		} else if line.to_lower() == "id" {
			mut id := os.input("id:")

			to_send_message := create_message_from_bytes(.request_id, number, id.bytes(), buffers, keys) or {
				println("failed to create the message to the server")
				continue
			}

			number++

			answer := client_side_send_and_wait(mut conn, to_send_message, [.recipe_response, .not_found]) or {
				if err.code() == error_message { return err }
				println("failed to receive message")
				continue
			}

			if answer.message_type == .not_found {
				println("recipe not found")
				continue
			}

			print("Recipe:\n\n\n\n")
			print(answer.text())
			print("\n\n\n\n")
		} else {
			print("Unknown command. Type:\n")
			print("q or quit - to quit\n")
			print("name      - to search by name\n")
			print("id        - to get a reciepe\n")
		}
	}
}
