module main

import os.cmdline
import os

fn main() {

	if '-h' in os.args || '--help' in os.args {
		println("Usage:\n\tTo only run the server:\n\t\t./cw -os\n\tTo only run the client:\n\t\t./cw -oc")
		println("\tTo get the help screen:\n\t\t./cw [-h | --help]\n\tTo change the port:\n\t\t./cw -p [port]")
	}

	port := cmdline.option(os.args, '-p', default_port).int()
	run_client := '--only-client' in os.args || '-oc' in os.args
	run_server := '--only-server' in os.args || '-os' in os.args
	ch := chan bool{}

	if run_client {
		client() or {println(err)}
	} else if run_server {
		server(port, true, ch) or {println(err)}
	} else {
		c := go client()
		s := go server(port, false, ch)
		c.wait() or {println(err)}
		ch <- false
		s.wait() or {}
	}
}
