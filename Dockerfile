FROM archlinux

RUN pacman --noconfirm -Sy base-devel git libsodium

RUN git clone https://github.com/andr3h3nriqu3s11/v.git

WORKDIR v

RUN git switch public-address-str

RUN make

RUN ./v symlink

WORKDIR /

RUN v install libsodium

WORKDIR /home

COPY v_op code
COPY v_op/recipes recipes

WORKDIR /home/code

# Remove the cw file if it exists 
RUN if [ -e "./cw" ]; then rm cw; fi

RUN v . -o cw

RUN mv cw ..

WORKDIR /home

EXPOSE 42069

CMD ["./cw"]
