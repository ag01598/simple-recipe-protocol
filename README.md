# Simple Recipe Protocol

This repository contains variuos test implementations of the simple recipe protocol.
 
The final implementaion and complete implementation is the [vlang](vlang.io) to run the code please run the Docker Image provided.

To run the final version please run(, note this will run the client and the server):

```bash
docker build . -t ag01598com2022

docker run ag01598com2022
```

To run with more options run
```bash
docker run ag01598com2022 bash
```

Then run to run just the server
```bash
./cw -os
```

Then run to run just the client to run just the client
```bash
./cw -oc
```

## RFC

The RFC folder contains the tex files that are used to generate the rfc

## Vlang Version

This is the ofical implementaion for the simple recipe protocol.

To run this program please use the Docker Image provided

